INSERT INTO jdbc.developers (id, firstName, lastName, specialty, salary) VALUES (UUID(), 'Ilya','Slysarenko', 'Java Developer', 1000);
INSERT INTO jdbc.developers (id, firstName, lastName, specialty, salary) VALUES (UUID(), 'Denis','Grammakov', 'C Developer', 1500);
INSERT INTO jdbc.developers (id, firstName, lastName, specialty, salary) VALUES (UUID(), 'Yaroslava','Gagen-Torn', 'C++ Developer', 800);
INSERT INTO jdbc.developers (id, firstName, lastName, specialty, salary) VALUES (UUID(), 'Anatoly','Karpenko', 'Pyton Developer', 1200);
INSERT INTO jdbc.developers (id, firstName, lastName, specialty, salary) VALUES (UUID(), 'Youra','Karpenko', 'C# Developer', 980);
INSERT INTO jdbc.developers (id, firstName, lastName, specialty, salary) VALUES (UUID(), 'Youra','Krohmal', 'JavaScript Developer', 700);