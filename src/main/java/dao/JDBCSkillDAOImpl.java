package dao;

import model.Skill;

import java.util.Set;
import java.util.UUID;

public class JDBCSkillDAOImpl implements SkillDAO{

    ConnectionUtil connectionUtil = new ConnectionUtil();

    public void insert(Skill skill) {
        String sqlSave = "INSERT INTO jdbc.skills VALUES ('" + skill.getId() + "', '" + skill.getName() + "');";
        connectionUtil.insertUpdateDelete(sqlSave);
    }

    public void update(Skill skill) {
        String sqlUpdate = "UPDATE jdbc.skills SET name = '" + skill.getName() + "' WHERE id = '" + skill.getId() + "';";
        connectionUtil.insertUpdateDelete(sqlUpdate);
    }

    public void delete(UUID id) {
        String sqlDelete = "DELETE FROM jdbc.skills WHERE id = '" + id + "';";
        connectionUtil.insertUpdateDelete(sqlDelete);
    }

    public Skill getById(UUID id) {
        String sqlSelect = "SELECT id, name FROM jdbc.skills WHERE id = " + id + ";";
        return connectionUtil.selectSkill(sqlSelect);
    }

    public Skill getByName(String name) {
        String sqlSelect = "SELECT id, name FROM jdbc.skills WHERE name = '" + name + "';";
        return connectionUtil.selectSkill(sqlSelect);
    }

    public Set<Skill> getAll() {
        String sqlSelect = "SELECT * FROM jdbc.skills;";
        return connectionUtil.selectAllSkills(sqlSelect);
    }
}
