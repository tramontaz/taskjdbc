package test.DAOTests;

import dao.JDBCSkillDAOImpl;
import dao.SkillDAO;
import model.Skill;

import java.util.Set;

public class SkillsDAOTest {

    public static void main(String[] args) {
        SkillDAO skillDAO = new JDBCSkillDAOImpl();
//        selectSkillByName(skillDAO);
//        insertNewSkill(skillDAO);
//        updateSkill(skillDAO);
//        deleteSkill(skillDAO);
//        selectAllSkills(skillDAO);
    }

    private static void selectAllSkills(SkillDAO skillDAO) {
        Set<Skill> skills = skillDAO.getAll();
        System.out.println(skills.toString());
    }

    private static void deleteSkill(SkillDAO skillDAO) {
        Skill deceive = new Skill("Deceive");
        skillDAO.insert(deceive);
        System.out.println(skillDAO.getByName("Deceive"));
        skillDAO.delete(deceive.getId());
    }

    private static void updateSkill(SkillDAO skillDAO) {
        Skill jdbcSkill = new Skill("JDBC");
        skillDAO.insert(jdbcSkill);
        jdbcSkill.setName("Java JDBC");
        skillDAO.update(jdbcSkill);
    }

    private static void insertNewSkill(SkillDAO skillDAO) {
        Skill newSkill = new Skill("UUID");
        skillDAO.insert(newSkill);
    }

    private static void selectSkillByName(SkillDAO skillDAO) {
        Skill skill = skillDAO.getByName("Java");
        System.out.println(skill);
    }
}
