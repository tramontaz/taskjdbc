package dao;

import model.Developer;
import model.Skill;

import java.util.Set;
import java.util.UUID;

public class JDBCDeveloperDAOImpl implements DeveloperDAO {
    ConnectionUtil connectionUtil = new ConnectionUtil();

    public void insert(Developer developer) {
        String devSave = "INSERT INTO jdbc.developers VALUES ('" + developer.getId() + "', '" + developer.getFirstName()
                + "', '" + developer.getLastName() + "', '" + developer.getSpecialty() + "', " + developer.getSalary() + ");";

        connectionUtil.insertUpdateDelete(devSave);
        connectionUtil.addSkillsToDev(developer.getId(), developer.getSet());
    }

    public void update(Developer developer) {
        Set<Skill> newSkills = developer.getSet();
        Set<Skill> oldSkills = connectionUtil.selectSkillsForDev(developer.getId());
        String updDev = "UPDATE jdbc.developers SET " +
                "firstName = '" + developer.getFirstName() + "', " +
                "lastName = '" + developer.getLastName() + "', " +
                "specialty = '" + developer.getSpecialty() + "', " +
                "salary = " + developer.getSalary() + " " +
                "WHERE id = '" + developer.getId() + "';";
        connectionUtil.insertUpdateDelete(updDev);

        if (!newSkills.equals(oldSkills)) {
            //add new skills
            for (Skill newSkill : newSkills) {
                if (!oldSkills.contains(newSkill)) {
                    connectionUtil.addThisSkillToDev(developer.getId(), newSkill);
                }
            }

            //delete old skills
            for (Skill oldSkill : oldSkills) {
                if (!newSkills.contains(oldSkill)) {
                    connectionUtil.deleteThisSkillFromDev(developer.getId(), oldSkill);
                }
//            if (!oldSkills.contains(skill) || newSkills.size()!=oldSkills.size()){
//                connectionUtil.deleteSkillsFromDevSkills(developer);
//                connectionUtil.addSkillsToDev(developer.getId(), newSkills);
//            }
//            connectionUtil.addSkillsToDev(developer.getId(), newSkills);
            }
        }
    }


    public void delete(UUID id) {
        String deleteByID = "DELETE FROM jdbc.developers WHERE id = '" + id + "';";
        connectionUtil.insertUpdateDelete(deleteByID);
        connectionUtil.deleteSkillsFromDevSkills(id);
    }

    public Developer getById(UUID id) {
        return null;
    }

    public Developer getByName(String firstName, String lastName) {
        String selectByName = "SELECT * FROM jdbc.developers WHERE firstName = '" + firstName + "' AND lastName = '" + lastName + "';";
        return connectionUtil.selectDev(selectByName);
    }

    public Set<Developer> getAll() {
        return null;
    }

    public Set<Skill> getDevelopersSkills() {
        return null;
    }

    public Skill getSkillByNameFrom_Skills(String name) {
        String sqlSelect = "SELECT id, name FROM jdbc.skills WHERE name = '" + name + "';";
        return connectionUtil.selectSkill(sqlSelect);
    }
}
