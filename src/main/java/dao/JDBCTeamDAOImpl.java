package dao;

import model.Developer;
import model.Team;

import java.util.Set;
import java.util.UUID;

public class JDBCTeamDAOImpl implements TeamDAO {
    ConnectionUtil connectionUtil = new ConnectionUtil();
    public void insert(Team team) {
        String devSave = "INSERT INTO jdbc.teams VALUES ('" + team.getId() + "', '" + team.getName() + "');";

        connectionUtil.insertUpdateDelete(devSave);
        connectionUtil.addDevToTeam(team.getId(), team.getSet());
    }

    public void update(Team team) {
        Set<Developer> newDevelopers = team.getSet();
        Set<Developer> oldDevelopers = connectionUtil.selectDevelopersToTeam(team.getId());
        String updTeam = "UPDATE jdbc.teams SET " +
                "name = '" + team.getName() + "';";
        connectionUtil.insertUpdateDelete(updTeam);

        for (Developer developer : newDevelopers) {
//            if (newDevelopers.size() < oldDevelopers.size()){
//                connectionUtil.deleteDevFromDevInTeams(team);
//                connectionUtil.addDevToTeam(developer.getId(), newDevelopers);
//        }
//                connectionUtil.deleteDevFromDevInTeams(team);
                connectionUtil.addDevToTeam(developer.getId(), newDevelopers);

        }
    }

    public void delete(UUID id) {

    }

    public Team getById(UUID id) {
        return null;
    }

    public Team getByName(String firstName, String lastName) {
        return null;
    }

    public DeveloperDAO getDeveloperByNameFrom_Developers(String name) {
        return null;
    }

    public Set<Team> getAll() {
        return null;
    }
}
