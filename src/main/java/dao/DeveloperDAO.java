package dao;

import model.Developer;
import model.Skill;

import java.util.Set;
import java.util.UUID;

public interface DeveloperDAO {
    void insert(Developer developer);
    void update(Developer developer);
    void delete(UUID id);
    Developer getById(UUID id); //select
    Developer getByName(String firstName, String lastName); //select
    Skill getSkillByNameFrom_Skills(String name);
    Set<Developer> getAll();
}
