package dao;

import model.Skill;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public interface SkillDAO {
    void insert(Skill skill);
    void update(Skill skill);
    void delete(UUID id);
    Skill getById(UUID id); //select
    Skill getByName(String name); //select
    Set<Skill> getAll();
}
