package test.DAOTests;

import dao.DeveloperDAO;
import dao.JDBCDeveloperDAOImpl;
import model.Developer;
import model.Skill;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class DeveloperDAOTest {

    public static void main(String[] args) {
        DeveloperDAO developerDAO = new JDBCDeveloperDAOImpl();
//        insertDev(developerDAO);
//        selectDevByName(developerDAO);
        updateDev(developerDAO);
//        deleteDev(developerDAO);

    }

    private static void deleteDev(DeveloperDAO developerDAO) {
        developerDAO.delete(developerDAO.getByName("Vadimka","Chemodurov").getId());
    }

    private static void updateDev(DeveloperDAO developerDAO) {
        Developer vadimius = developerDAO.getByName("Vadim", "Chemodurov");
        Set<Skill> devSkills = new HashSet<Skill>();
        devSkills.add(developerDAO.getSkillByNameFrom_Skills("Java"));
        devSkills.add(developerDAO.getSkillByNameFrom_Skills("UUID"));
        devSkills.add(developerDAO.getSkillByNameFrom_Skills("BigData"));

        vadimius.setFirstName("Vadimka");
        vadimius.setSet(devSkills);
        developerDAO.update(vadimius);
    }

    private static void selectDevByName(DeveloperDAO developerDAO) {
        Developer newDev = developerDAO.getByName("Vadim", "Chemodurov");
        System.out.println(newDev.toString());
    }

    private static void insertDev(DeveloperDAO developerDAO) {
        Set<Skill> devSkills = new HashSet<Skill>();
        devSkills.add(developerDAO.getSkillByNameFrom_Skills("Java"));
        devSkills.add(developerDAO.getSkillByNameFrom_Skills("UUID"));

        Developer developer = new Developer("Vadim", "Chemodurov", "Java Developer", devSkills, new BigDecimal(1000));

        developerDAO.insert(developer);
    }

}
