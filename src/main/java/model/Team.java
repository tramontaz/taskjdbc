package model;

import java.util.Set;
import java.util.UUID;

public class Team {
    private UUID id;
    private String name;
    private Set<Developer> set;


    public Team(UUID id, String name, Set<Developer> set) {
        this.id = id;
        this.name = name;
        this.set = set;
    }

    public Team(String name, Set<Developer> set) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.set = set;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Developer> getSet() {
        return set;
    }

    public void setSet(Set<Developer> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "id=" + id +
                "\nname=" + name +
                "\nDevelopers set=" + set;
    }
}
