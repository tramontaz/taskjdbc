package dao;

import model.Developer;
import model.Skill;
import model.Team;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ConnectionUtil {

    /**
     * driver, url, login and password.
     */
    final String DB_URL = "jdbc:mysql://localhost/jdbc";
    final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    final String USERNAME = "jdbc";
    final String PASSWORD = "%JDBC_tutorial0";

    public void insertUpdateDelete(String sql) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            statement.executeUpdate(sql);

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //for Skills
    public Skill selectSkill(String sql) {
        Skill skill = null;
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next())
                skill = new Skill(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"));
            else throw new Exception("There is no skill with that id...");

            //close resultSet
            resultSet.close();
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return skill;
    }



    //for developer

    //get String "sql query" and return Developer
    public Developer selectDev(String sql) {
        Developer developer = null;
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next())
                developer = new Developer(UUID.fromString(resultSet.getString("id")), resultSet.getString("firstName"),
                        resultSet.getString("lastName"), resultSet.getString("specialty"),
                        selectSkillsForDev(UUID.fromString(resultSet.getString("id"))),
                        resultSet.getBigDecimal("salary"));
            else throw new Exception("There is no developer with that id...");

            //close resultSet
            resultSet.close();
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return developer;
    }
    //get developers's UUID and skillSet for inserting into devSkills table all developer's skills
    public void addSkillsToDev(UUID dev_id, Set<Skill> skillSet) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.prepareStatement("INSERT INTO devSkills (dev_id, skill_id) " +
                    "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                    "WHERE NOT EXISTS (SELECT * FROM devSkills WHERE dev_id = ? AND skill_id = ?) LIMIT 1;");
            for (Skill aSkillSet : skillSet) {
                statement.setString(1, String.valueOf(dev_id));
                statement.setString(2, String.valueOf(aSkillSet.getId()));
                statement.setString(3, String.valueOf(dev_id));
                statement.setString(4, String.valueOf(aSkillSet.getId()));
                //add query to batch
                statement.addBatch();
            }
            // run all queries
            statement.executeBatch();

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //delete one skill from developer into devSkill table
    public void deleteThisSkillFromDev(UUID id, Skill skill) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.prepareStatement("DELETE FROM devSkills WHERE dev_id = ? AND skill_id = ?;");
                statement.setString(1, String.valueOf(id));
                statement.setString(2, String.valueOf(skill.getId()));
            // run all queries
            statement.executeUpdate();

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //insert one skill to developer into devSkill table
    public void addThisSkillToDev(UUID id, Skill newSkill) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.prepareStatement("INSERT INTO devSkills (dev_id, skill_id) VALUES (?, ?);");
            statement.setString(1, String.valueOf(id));
            statement.setString(2, String.valueOf(newSkill.getId()));
            // run all queries
            statement.executeUpdate();

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //delete all skills from developer into devSkill table by developer's UUID
    public void deleteSkillsFromDevSkills(UUID devUUID){
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM jdbc.devSkills WHERE dev_id = '" + devUUID + "';");

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //delete all skills from developer into devSkill table by developer
    public void deleteSkillsFromDevSkills(Developer developer){
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM jdbc.devSkills WHERE dev_id = '" + developer.getId() + "';");

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //get developer's skills from devSkill table
    public Set<Skill> selectSkillsForDev(UUID devUIID) {
        Set<Skill> skillSet = new HashSet<Skill>();
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            String sql = "SELECT skill_id, skills.name FROM jdbc.devSkills, jdbc.skills WHERE dev_id = '" + devUIID + "' AND skill_id = skills.id;";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet != null) {
                while (resultSet.next()) {
                    skillSet.add(new Skill(UUID.fromString(resultSet.getString("skill_id")), resultSet.getString("name")));
                }
            } else throw new Exception("There is no skills...");
            //close resultSet
            resultSet.close();
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return skillSet;
    }



    //for team
    public void addDevToTeam(UUID team_id, Set<Developer> devSet) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.prepareStatement("INSERT INTO jdbc.devInTeams  VALUES (?,?)");
            for (Developer aDevSet : devSet) {
                statement.setString(1, String.valueOf(team_id));
                statement.setString(2, String.valueOf(aDevSet.getId()));
                //add query to batch
                statement.addBatch();
            }
            // run all queries
            statement.executeBatch();

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }



    public Set<Skill> selectAllSkills(String sql) {
        Set<Skill> skillSet = new HashSet<Skill>();
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet != null) {
                while (resultSet.next()) {
                    skillSet.add(new Skill(UUID.fromString(resultSet.getString("id")), resultSet.getString("name")));
                }
            } else throw new Exception("There is no skills...");
            //close resultSet
            resultSet.close();
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return skillSet;
    }



    public Set<Developer> selectDevelopersToTeam(UUID id) {
        Set<Developer> devSet = new HashSet<Developer>();
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            String sql = "SELECT dev_id, developers.firstName, developers.lastName, developers.specialty, developers.salary" +
                    " FROM jdbc.devInTeams, jdbc.developers WHERE team_id = '" + id + "' AND dev_id = developers.id;";


            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet != null) {
                while (resultSet.next()) {
                    devSet.add(new Developer(
                            UUID.fromString(resultSet.getString("dev_id")),
                            resultSet.getString("firstName"),
                            resultSet.getString("lastName"),
                            resultSet.getString("specialty"),
                            selectSkillsForDev(UUID.fromString(resultSet.getString("dev_id"))),
                            resultSet.getBigDecimal("salary")));
                }
            } else throw new Exception("There is no developers...");
            //close resultSet
            resultSet.close();
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return devSet;
    }

    public void deleteDevFromDevInTeams(Team team) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Register JDBC Driver:
            Class.forName(JDBC_DRIVER);

            //Open a connection:
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            //Execute a query:
            statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM jdbc.devInTeams WHERE team_id = '" + team.getId() + "';");

        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        } finally {
            //close resources:
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
