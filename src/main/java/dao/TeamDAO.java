package dao;

import model.Team;

import java.util.Set;
import java.util.UUID;

public interface TeamDAO {
    void insert(Team team);

    void update(Team team);

    void delete(UUID id);

    Team getById(UUID id); //select

    Team getByName(String firstName, String lastName); //select

    DeveloperDAO getDeveloperByNameFrom_Developers(String name);

    Set<Team> getAll();
}