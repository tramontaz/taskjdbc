package test.DAOTests;

import dao.DeveloperDAO;
import dao.JDBCDeveloperDAOImpl;
import dao.JDBCTeamDAOImpl;
import dao.TeamDAO;
import model.Developer;
import model.Team;

import java.util.HashSet;
import java.util.Set;

public class TeamDAOTest {
    public static void main(String[] args) {
        DeveloperDAO developerDAO = new JDBCDeveloperDAOImpl();
        TeamDAO teamDAO = new JDBCTeamDAOImpl();
        Set<Developer> alfhaDevs = new HashSet<Developer>();
        alfhaDevs.add(developerDAO.getByName("Ilya", "Slysarenko"));
        alfhaDevs.add(developerDAO.getByName("Denis", "Grammakov"));
//        insertTeamToDB(teamDAO, alfhaDevs);

        Team alpha = new Team("alpha", alfhaDevs);
        System.out.println(alpha.toString());

        teamDAO.insert(alpha);

        alpha.setName("imNotAlphaAnymore");

        teamDAO.update(alpha);

    }

    private static void insertTeamToDB(TeamDAO teamDAO, Set<Developer> alfhaDevs) {
        Team alpha = new Team("alpha", alfhaDevs);
        System.out.println(alpha.toString());

        teamDAO.insert(alpha);
    }
}
